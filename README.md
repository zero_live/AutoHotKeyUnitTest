# AutoHotKey UnitTest Library

Little library for do AutoHotKey scripts unitary tests.

## System requirements

- AutoHotKey v1.1 or higher.

## Usage

- Include the file `UnitTest.ahk` in your script test.
- Create a class for defeine what you want to test and extend it from `UnitTest`.
- Create the method `tests` in your class (inside of this methods you can write your tests).
- In `tests` method create a variable for explain what you are testing, write your stuff, for finally your test use `This.assertEqual(<gotValue>,<expectedValue>,<testDefinition>)`
- And finally at the end of the file call your class and execute `run` method.

Example:

```
#Include ../UnitTest.ahk

class TestExample extends UnitTest {
  tests() {
    it = "Test pass"

    got = 1
    expected = 1
    This.assertEqual(got, expected, it)

    it = "Test fails"

    got = 1
    expected = 2
    This.assertEqual(got, expected, it)
  }
}

TestExample.run()
```
