
class UnitTest {
  showError(got, expected, testName) {
    MsgBox, , UnitTest,
    (
FAILED: %testName%
GOT: %got%
EXPECTED: %expected%
    )
  }

  stopTesting() {
    Exit
  }

  assertEqual(got, expected, testName) {
    if (got != expected) {
      This.showError(got, expected, testName)
      This.stopTesting()
    }
  }

  testsPass() {
    MsgBox, , UnitTest, "All test passed"
  }

  run() {
    This.tests()
    This.testsPass()
    This.stopTesting()
  }

  tests() {
    MsgBox, , UnitTest, "tests method has to be implemented"
    This.stopTesting()
  }
}
